package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	ipclient "git.autistici.org/ai3/tools/iprep/client"
	"github.com/google/subcommands"
	"google.golang.org/grpc"
)

type dumpCommand struct {
	clientBase
}

func (c *dumpCommand) Name() string     { return "dump" }
func (c *dumpCommand) Synopsis() string { return "dump all scores" }
func (c *dumpCommand) Usage() string {
	return `dump [<flags>]:

        Dump all the IPs and scores contained in the database.
`
}

func (c *dumpCommand) SetFlags(f *flag.FlagSet) {
	c.clientBase.SetFlags(f)
}

func (c *dumpCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}
	if c.serverAddr == "" {
		log.Printf("error: must specify --server")
		return subcommands.ExitUsageError
	}

	if err := c.run(ctx); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func (c *dumpCommand) run(ctx context.Context) error {
	opts, err := c.clientBase.ClientOpts()
	if err != nil {
		return err
	}
	conn, err := grpc.Dial(c.serverAddr, opts...)
	if err != nil {
		return err
	}
	defer conn.Close()

	client := ipclient.New(conn)

	ch, err := client.GetAllScores(ctx, 0)
	if err != nil {
		return err
	}

	for entry := range ch {
		fmt.Printf("%s %f\n", entry.Ip, entry.Score)
	}

	return nil
}

func init() {
	subcommands.Register(&dumpCommand{}, "")
}
