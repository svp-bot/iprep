package main

import (
	"bufio"
	"context"
	"flag"
	"log"
	"os"
	"strconv"
	"strings"

	ipclient "git.autistici.org/ai3/tools/iprep/client"
	ippb "git.autistici.org/ai3/tools/iprep/proto"
	"github.com/google/subcommands"
	"google.golang.org/grpc"
)

type submitCommand struct {
	clientBase
}

func (c *submitCommand) Name() string     { return "submit" }
func (c *submitCommand) Synopsis() string { return "submit events" }
func (c *submitCommand) Usage() string {
	return `submit [<flags>]:

        Submit events from standard input. The format is very simple
        and consists of one line per event, with IP address, event
        type, and an optional count (default is 1 if omitted) separated
        by spaces.

`
}

func (c *submitCommand) SetFlags(f *flag.FlagSet) {
	c.clientBase.SetFlags(f)
}

func (c *submitCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}
	if c.serverAddr == "" {
		log.Printf("error: must specify --server")
		return subcommands.ExitUsageError
	}

	if err := c.run(ctx); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func (c *submitCommand) run(ctx context.Context) error {
	opts, err := c.clientBase.ClientOpts()
	if err != nil {
		return err
	}
	conn, err := grpc.Dial(c.serverAddr, opts...)
	if err != nil {
		return err
	}
	defer conn.Close()

	client := ipclient.New(conn)

	var events []*ippb.Event
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := strings.Fields(scanner.Text())
		if len(line) < 2 || len(line) > 3 {
			log.Printf("syntax error")
			continue
		}

		event := &ippb.Event{
			Ip:    line[0],
			Type:  line[1],
			Count: 1,
		}
		if len(line) == 3 {
			var err error
			event.Count, err = strconv.ParseInt(line[2], 10, 64)
			if err != nil {
				log.Printf("syntax error")
				continue
			}
		}
		events = append(events, event)
	}

	return client.Submit(ctx, events, nil)
}

func init() {
	subcommands.Register(&submitCommand{}, "")
}
