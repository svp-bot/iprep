//go:generate go-bindata --nocompress --pkg sqlite --ignore \.go$ -o bindata.go -prefix migrations/ ./migrations
package sqlite

import (
	"database/sql"
	"log"

	migrate "github.com/golang-migrate/migrate/v4"
	msqlite3 "github.com/golang-migrate/migrate/v4/database/sqlite3"
	bindata "github.com/golang-migrate/migrate/v4/source/go_bindata"
	_ "github.com/mattn/go-sqlite3"
)

const dbDriver = "sqlite3"

var DebugMigrations = false

// Open a SQLite database and run the database migrations.
func sqlOpen(dburi string) (*sql.DB, error) {
	db, err := sql.Open(dbDriver, dburi)
	if err != nil {
		return nil, err
	}

	if err = runDatabaseMigrations(db); err != nil {
		db.Close() // nolint
		return nil, err
	}

	return db, nil
}

type migrateLogger struct{}

func (l migrateLogger) Printf(format string, v ...interface{}) {
	log.Printf("db: "+format, v...)
}

func (l migrateLogger) Verbose() bool { return true }

func runDatabaseMigrations(db *sql.DB) error {
	si, err := bindata.WithInstance(bindata.Resource(
		AssetNames(),
		func(name string) ([]byte, error) {
			return Asset(name)
		}))
	if err != nil {
		return err
	}

	di, err := msqlite3.WithInstance(db, &msqlite3.Config{
		MigrationsTable: msqlite3.DefaultMigrationsTable,
		DatabaseName:    "iprep",
	})
	if err != nil {
		return err
	}

	m, err := migrate.NewWithInstance("bindata", si, dbDriver, di)
	if err != nil {
		return err
	}
	if DebugMigrations {
		log.Printf("running database migrations")
		m.Log = &migrateLogger{}
	}

	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return err
	}
	return nil
}
