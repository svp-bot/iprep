package db

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net"
	"os"
	"sync"
	"testing"
	"time"

	"git.autistici.org/ai3/tools/iprep/db/leveldb"
	"git.autistici.org/ai3/tools/iprep/db/sqlite"
	ippb "git.autistici.org/ai3/tools/iprep/proto"
	"github.com/google/go-cmp/cmp"
)

const (
	// How many events to fill up the db, test/bench cases.
	manyEventsTest  = 100000
	manyEventsBench = 1000000

	// When writing events to the db, do it in batches of this size.
	writeBatchSize = 1000
)

func randomIP() string {
	var ip net.IP

	// Choose between IPv4/IPv6 with a certain frequency.
	if rand.Intn(10) < 3 {
		for i := 0; i < net.IPv6len; i++ {
			ip = append(ip, byte(rand.Intn(255)))
		}
	} else {
		ip = net.IPv4(byte(1+rand.Intn(220)), byte(rand.Intn(256)),
			byte(rand.Intn(256)), byte(rand.Intn(256)))
	}
	return ip.String()
}

func randomIPs(n int) []string {
	out := make([]string, n)
	for i := 0; i < n; i++ {
		out[i] = randomIP()
	}
	return out
}

var eventTypes = []string{
	"detection1", "detection2", "detection3", "detection4", "detection5",
	"detection6", "detection7", "detection8", "detection9", "detection10",
}

func randomType() string {
	return eventTypes[rand.Intn(len(eventTypes))]
}

func randomEvent() *ippb.Event {
	return &ippb.Event{
		Ip:    randomIP(),
		Type:  randomType(),
		Count: 1,
	}
}

func randomEvents(n int) *ippb.Aggregate {
	aggr := new(ippb.Aggregate)
	for i := 0; i < n; i++ {
		aggr.AddEvent(randomEvent())
	}
	return aggr
}

func createTestDB(t testing.TB, driver string) (DB, func()) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}

	db, err := Open(fmt.Sprintf("%s://%s", driver, dir+"/test.db"),
		ippb.DefaultIPMask)
	if err != nil {
		t.Fatalf("Open: %v", err)
	}

	return db, func() {
		db.Close()
		os.RemoveAll(dir)
	}
}

var testEvents = []*ippb.Event{
	&ippb.Event{
		Ip:    "1.2.3.4",
		Type:  "spam",
		Count: 1,
	},
	&ippb.Event{
		Ip:    "1.2.3.4",
		Type:  "spam",
		Count: 1,
	},
	&ippb.Event{
		Ip:    "2.3.4.5",
		Type:  "spam",
		Count: 1,
	},
}

func runSanityCheck(t *testing.T, driver string) {
	db, cleanup := createTestDB(t, driver)
	defer cleanup()

	events := new(ippb.Aggregate)
	for _, e := range testEvents {
		events.AddEvent(e)
	}
	if err := db.AddAggregate(events); err != nil {
		t.Fatalf("AddEvents: %v", err)
	}

	expect := func(ip, eventType string, count int64) {
		m, err := db.ScanIP(time.Now().Add(-1*time.Hour), ip)
		if err != nil {
			t.Fatalf("ScanIP: %v", err)
		}
		if n := m[eventType]; n != count {
			t.Fatalf("(%s/%s): read %d events from db, expected %d", ip, eventType, n, count)
		}
	}

	expect("1.2.3.4", "spam", 2)
	expect("2.3.4.5", "spam", 1)
	expect("3.4.5.6", "spam", 0)
	expect("1.2.3.4", "not-spam", 0)
}

func TestSanity_LevelDB(t *testing.T) {
	runSanityCheck(t, "leveldb")
}

func TestSanity_Sqlite(t *testing.T) {
	runSanityCheck(t, "sqlite")
}

func runRWTest(t *testing.T, driver string) {
	db, cleanup := createTestDB(t, driver)
	defer cleanup()

	// Write n elements.
	n := manyEventsTest
	events := randomEvents(n)
	if err := db.AddAggregate(events); err != nil {
		t.Fatalf("AddEvents: %v", err)
	}

	//data, _ := exec.Command("du", "-sh", dir).Output()
	//t.Logf("added %d records: %s", n, string(data))

	// Scan events from 1 hour in the past, for the first IP in
	// events, should fetch a non-zero number of events.
	refType := events.ByType[0].Type
	refIP := events.ByType[0].ByIp[0].Ip
	m, err := db.ScanIP(time.Now().Add(-1*time.Hour), refIP)
	if err != nil {
		t.Fatalf("ScanIP(%s): %v", refIP, err)
	}
	if count := m[refType]; count < 1 {
		t.Fatalf("read %d events from db, expected > 0", count)
	}

	// Scan events from 1 hour in the future, should return 0 results.
	// count = 0
	// if err := db.Scan(time.Now().Add(1*time.Hour), func(*ippb.Aggregate) error {
	// 	return errors.New("unexpected Aggregate")
	// }, func(*ippb.Event) error {
	// 	count++
	// 	return nil
	// }); err != nil {
	// 	t.Fatalf("Scan: %v", err)
	// }
	// if count != 0 {
	// 	t.Fatalf("scan in the future read %d events from db, expected 0", count)
	// }
}

func TestReadWrite_LevelDB(t *testing.T) {
	runRWTest(t, "leveldb")
}

func TestReadWrite_Sqlite(t *testing.T) {
	runRWTest(t, "sqlite")
}

func runScanAllTest(t *testing.T, driver string) {
	db, cleanup := createTestDB(t, driver)
	defer cleanup()

	events := new(ippb.Aggregate)
	for _, e := range testEvents {
		events.AddEvent(e)
	}
	if err := db.AddAggregate(events); err != nil {
		t.Fatalf("AddEvents: %v", err)
	}

	m := make(map[string]map[string]int64)
	err := db.ScanAll(time.Now().Add(-1*time.Hour), func(ip string, counts map[string]int64) error {
		m[ip] = counts
		return nil
	})
	if err != nil {
		t.Fatalf("ScanAll: %v", err)
	}

	expected := map[string]map[string]int64{
		"1.2.3.4": map[string]int64{
			"spam": 2,
		},
		"2.3.4.5": map[string]int64{
			"spam": 1,
		},
	}

	if diffs := cmp.Diff(expected, m); diffs != "" {
		t.Fatalf("unexpected result:\n%s", diffs)
	}
}

func TestScanAll_LevelDB(t *testing.T) {
	runScanAllTest(t, "leveldb")
}

func TestScanAll_Sqlite(t *testing.T) {
	runScanAllTest(t, "sqlite")
}

func runWipeOldDataTest(t *testing.T, driver string) {
	db, cleanup := createTestDB(t, driver)
	defer cleanup()

	if err := db.AddAggregate(randomEvents(100)); err != nil {
		t.Fatalf("AddEvents: %v", err)
	}
	n, err := db.WipeOldData(0)
	if err != nil {
		t.Fatalf("WipeOldData: %v", err)
	}
	if n == 0 {
		t.Fatal("WipeOldData returned 0 records")
	}
}

func TestWipeOldData_LevelDB(t *testing.T) {
	runWipeOldDataTest(t, "leveldb")
}

func TestWipeOldData_Sqlite(t *testing.T) {
	runWipeOldDataTest(t, "sqlite")
}

func runWriteBench(b *testing.B, driver string) {
	db, cleanup := createTestDB(b, driver)
	defer cleanup()

	// Write events in chunks.
	nchunks := b.N / writeBatchSize
	events := randomEvents(writeBatchSize)

	b.ResetTimer()

	for i := 0; i < nchunks; i++ {
		if err := db.AddAggregate(events); err != nil {
			b.Fatalf("AddEvents: %v", err)
		}
	}
}

func BenchmarkWrite_LevelDB(b *testing.B) {
	runWriteBench(b, "leveldb")
}

func BenchmarkWrite_Sqlite(b *testing.B) {
	runWriteBench(b, "sqlite")
}

func runReadBenchmark(b *testing.B, driver string, eventsPerIP int, threadCounts []int) {
	db, cleanup := createTestDB(b, driver)
	defer cleanup()

	// Write a lot of events just to bulk up the database, but
	// also write a couple of records for a known IP, that we're
	// going to read in the benchmark. Do it in batches.
	for i := 0; i < manyEventsBench; i += writeBatchSize {
		db.AddAggregate(randomEvents(writeBatchSize))
	}

	// For a bunch of random IPs, add a known (possibly large)
	// number of events.
	numIPs := 100
	refIPs := randomIPs(numIPs)
	for _, ip := range refIPs {
		a := new(ippb.Aggregate)
		for i := 0; i < eventsPerIP; i++ {
			a.AddEvent(&ippb.Event{Type: randomType(), Ip: ip, Count: 1})
		}
		db.AddAggregate(a)
	}

	// Compact the database if it's LevelDB.
	if ldb, ok := db.(*leveldb.DB); ok {
		ldb.Compact()
	}

	deadline := time.Now().Add(-1 * time.Hour)

	fn := func(b *testing.B, off int) {
		ipCount := (off * 13) % len(refIPs)
		for i := 0; i < b.N; i++ {
			refIP := refIPs[ipCount]
			ipCount++
			if ipCount >= len(refIPs) {
				ipCount = 0
			}
			m, err := db.ScanIP(deadline, refIP)
			if err != nil {
				b.Fatalf("ScanIP(%d): %v", i, err)
			}
			if len(m) == 0 {
				b.Fatalf("ScanIP(%d): returned empty result", i)
			}
			if len(m) < 1 {
				b.Fatalf("ScanIP(%d): returned bad results: %v", i, m)
			}
		}
	}

	// Now run separate benchmarks for each of the threadCounts values.
	for _, numThreads := range threadCounts {
		nt := numThreads
		b.Run(fmt.Sprintf("threads:%d", nt), func(b *testing.B) {
			var wg sync.WaitGroup
			start := make(chan struct{})
			for i := 0; i < nt; i++ {
				wg.Add(1)
				go func(i int) {
					<-start
					fn(b, i)
					wg.Done()
				}(i)
			}
			close(start)
			wg.Wait()
		})
	}
}

var threadTests = []int{1, 5, 50}

func BenchmarkRead_LevelDB_SmallAggregate(b *testing.B) {
	runReadBenchmark(b, "leveldb", 5, threadTests)
}

func BenchmarkRead_LevelDB_LargeAggregate(b *testing.B) {
	runReadBenchmark(b, "leveldb", 1000, threadTests)
}

func BenchmarkRead_LevelDB_VeryLargeAggregate(b *testing.B) {
	runReadBenchmark(b, "leveldb", 10000, []int{1})
}

func BenchmarkRead_Sqlite_SmallAggregate(b *testing.B) {
	runReadBenchmark(b, "sqlite", 5, threadTests)
}

func BenchmarkRead_Sqlite_LargeAggregate(b *testing.B) {
	runReadBenchmark(b, "sqlite", 1000, threadTests)
}

func BenchmarkRead_Sqlite_VeryLargeAggregate(b *testing.B) {
	runReadBenchmark(b, "sqlite", 10000, []int{1})
}

func runScanAllBenchmark(b *testing.B, driver string, threadCounts []int) {
	db, cleanup := createTestDB(b, driver)
	defer cleanup()

	// Write a lot of events just to bulk up the database, but
	// also write a couple of records for a known IP, that we're
	// going to read in the benchmark. Do it in batches.
	for i := 0; i < manyEventsBench; i += writeBatchSize {
		db.AddAggregate(randomEvents(writeBatchSize))
	}

	// Compact the database if it's LevelDB.
	if ldb, ok := db.(*leveldb.DB); ok {
		ldb.Compact()
	}

	deadline := time.Now().Add(-1 * time.Hour)

	fn := func(b *testing.B, off int) {
		for i := 0; i < b.N; i++ {
			err := db.ScanAll(deadline, func(ip string, m map[string]int64) error {
				return nil
			})
			if err != nil {
				b.Fatalf("ScanAll: %v", err)
			}
		}
	}

	// Now run separate benchmarks for each of the threadCounts values.
	for _, numThreads := range threadCounts {
		nt := numThreads
		b.Run(fmt.Sprintf("threads:%d", nt), func(b *testing.B) {
			var wg sync.WaitGroup
			start := make(chan struct{})
			for i := 0; i < nt; i++ {
				wg.Add(1)
				go func(i int) {
					<-start
					fn(b, i)
					wg.Done()
				}(i)
			}
			close(start)
			wg.Wait()
		})
	}
}

func BenchmarkScanAll_LevelDB_LargeAggregate(b *testing.B) {
	runScanAllBenchmark(b, "leveldb", threadTests)
}

func init() {
	sqlite.DebugMigrations = true
}
