package leveldb

import (
	"encoding/binary"
	"sync"
	"time"
)

var uidState struct {
	mx      sync.Mutex
	ts      uint64
	counter uint16
}

func tsAndCounter() (uint64, uint16) {
	uidState.mx.Lock()
	defer uidState.mx.Unlock()

	cur := uint64(time.Now().UnixNano())
	if cur == uidState.ts {
		uidState.counter++
	} else {
		uidState.ts = cur
		uidState.counter = 0
	}
	return cur, uidState.counter
}

func nextID() []byte {
	ts, counter := tsAndCounter()

	b := make([]byte, 10)
	binary.BigEndian.PutUint64(b[:8], ts)
	binary.BigEndian.PutUint16(b[8:10], counter)

	return b
}
