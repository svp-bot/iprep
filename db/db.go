package db

import (
	"errors"
	"fmt"
	"net/url"
	"time"

	"git.autistici.org/ai3/tools/iprep/db/leveldb"
	"git.autistici.org/ai3/tools/iprep/db/sqlite"
	ippb "git.autistici.org/ai3/tools/iprep/proto"
)

type DB interface {
	AddAggregate(*ippb.Aggregate) error
	WipeOldData(time.Duration) (int64, error)
	ScanIP(time.Time, string) (map[string]int64, error)
	ScanType(time.Time, string) (map[string]int64, error)
	ScanAll(time.Time, func(string, map[string]int64) error) error
	Close()
}

func Open(path string, mask ippb.IPMask) (DB, error) {
	u, err := url.Parse(path)
	if err != nil {
		return nil, err
	}
	if u.Path == "" {
		return nil, errors.New("empty path in DB URI")
	}

	switch u.Scheme {
	case "", "leveldb":
		return leveldb.Open(u.Path, mask)
	case "sqlite":
		return sqlite.Open(u.Path, mask)
	default:
		return nil, fmt.Errorf("unsupported scheme %s", u.Scheme)
	}
}
