package proto

import (
	"fmt"
	"math/rand"
	"net"
	"testing"
)

func randomIP() string {
	return fmt.Sprintf("%d.%d.%d.%d", 1+rand.Intn(220), rand.Intn(256), rand.Intn(256), rand.Intn(256))
}

func randomType() string {
	return "detection"
}

func randomEvent() *Event {
	return &Event{
		Ip:   randomIP(),
		Type: randomType(),
	}
}

func randomEvents(n int) []*Event {
	out := make([]*Event, n)
	for i := 0; i < n; i++ {
		out[i] = randomEvent()
	}
	return out
}

func TestAggregate_FromEvents(t *testing.T) {
	evs := randomEvents(1000)
	aggr := MakeAggregate(evs)
	if len(aggr.ByType) == 0 {
		t.Fatal("no results from MakeAggregate")
	}

	count, ok := aggr.GetCount(evs[0].Type, evs[0].Ip)
	if !ok {
		t.Fatalf("couldn't find event in aggregate")
	}
	if count < 1 {
		t.Fatalf("count=0 in aggregate")
	}
}

func TestAggregate_Basics(t *testing.T) {
	a := &Aggregate{
		ByType: []*AggregateTypeEntry{
			&AggregateTypeEntry{
				Type: "test",
				ByIp: []*AggregateIPEntry{
					&AggregateIPEntry{
						Ip:    "1.2.3.4",
						Count: 2,
					},
				},
			},
		},
	}

	m := make(Map)
	m.Update(a)
	if n := m["test"]["1.2.3.4"]; n != 2 {
		t.Fatalf("error in Aggregate -> map, expected 2 but got %d", n)
	}
}

func TestAggregate_Merge(t *testing.T) {
	a := &Aggregate{
		ByType: []*AggregateTypeEntry{
			&AggregateTypeEntry{
				Type: "test",
				ByIp: []*AggregateIPEntry{
					&AggregateIPEntry{
						Ip:    "1.2.3.4",
						Count: 2,
					},
				},
			},
		},
	}
	b := &Aggregate{
		ByType: []*AggregateTypeEntry{
			&AggregateTypeEntry{
				Type: "test",
				ByIp: []*AggregateIPEntry{
					&AggregateIPEntry{
						Ip:    "1.2.3.4",
						Count: 3,
					},
				},
			},
		},
	}

	c := a.Merge(b)
	count, ok := c.GetCount("test", "1.2.3.4")
	if !ok {
		t.Fatal("no value in aggregate")
	}
	if count != 5 {
		t.Fatalf("bad acount in aggregate: expected 5, got %d", count)
	}
}

func TestIPMask(t *testing.T) {
	testdata := []struct {
		ip             string
		v4bits, v6bits int
		exp            string
	}{
		{"1.2.3.4", 32, 128, "1.2.3.4"},
		{"1.2.3.4", 16, 128, "1.2.0.0"},
		{"2001:a020:9f1d:c96:2efd:a1ff:fec6:f2fe", 32, 64, "2001:a020:9f1d:c96::"},
	}

	for _, td := range testdata {
		mask := NewIPMask(td.v4bits, td.v6bits)
		ip := net.ParseIP(td.ip)
		result := mask.MaskIP(ip)
		resultStr := mask.MaskString(td.ip)
		if result.String() != resultStr {
			t.Errorf(
				"internal inconsistency, MaskIP(%s/%d/%d) -> %s, MaskString -> %s",
				td.ip, td.v4bits, td.v6bits, result.String(), resultStr)
		}
		if resultStr != td.exp {
			t.Errorf(
				"MaskIP(%s/%d/%d) -> %s, expected %s",
				td.ip, td.v4bits, td.v6bits, resultStr, td.exp)
		}
	}
}
