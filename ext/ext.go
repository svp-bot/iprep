package ext

import (
	"errors"
	"fmt"

	tengo "github.com/d5/tengo/v2"

	"git.autistici.org/ai3/tools/iprep/ext/dnsbl"
	"git.autistici.org/ai3/tools/iprep/ext/geoip"
)

// An ExternalSource provides per-IP information from third-party
// sources. The lookup can return any Tengo object, we don't want to
// force a specific return type yet (int or string can both be
// useful, we'll see).
type ExternalSource interface {
	LookupIP(string) (tengo.Object, error)
}

// New creates a new ExternalSource.
func New(sourceType string, params map[string]interface{}) (ExternalSource, error) {
	switch sourceType {

	case "dnsbl":
		domain, ok := params["domain"].(string)
		if !ok {
			return nil, errors.New("missing parameter 'domain'")
		}
		match, ok := params["match"].(string)
		if !ok {
			match = ".*"
		}
		return dnsbl.New(domain, match)

	case "geoip":
		var paths []string
		if l, ok := params["paths"].([]interface{}); ok {
			for _, p := range l {
				paths = append(paths, p.(string))
			}
		}
		return geoip.New(paths)

	default:
		return nil, fmt.Errorf("unknown source type '%s'", sourceType)
	}
}
