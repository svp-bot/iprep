package dnsbl

import (
	"fmt"
	"net"
	"regexp"
	"strings"

	tengo "github.com/d5/tengo/v2"
)

// The DNSBL external source looks up IP addresses in a DNS-based
// blacklist, and expects the result to match a specific pattern.
type DNSBL struct {
	domain  string
	matchRx *regexp.Regexp
}

func New(domain, match string) (*DNSBL, error) {
	if match == "" {
		match = `^127\.0\.0\.[0-9]*$`
	}
	rx, err := regexp.Compile(match)
	if err != nil {
		return nil, err
	}
	return &DNSBL{
		domain:  domain,
		matchRx: rx,
	}, nil
}

func reverseIP(ip net.IP) string {
	if ip.To4() != nil {
		parts := strings.Split(ip.String(), ".")
		rev := make([]string, len(parts))
		for i := 0; i < len(parts); i++ {
			rev[len(parts)-i-1] = parts[i]
		}
		return strings.Join(rev, ".")
	}
	return ""
}

// LookupIP implements the ExternalSource interface. We'd like to
// return a boolean but can't figure out how to build one with Tengo,
// so we return a 0/1 int result instead.
func (d *DNSBL) LookupIP(ip string) (tengo.Object, error) {
	rev := reverseIP(net.ParseIP(ip))
	if rev == "" {
		return tengo.UndefinedValue, nil
	}
	query := fmt.Sprintf("%s.%s", rev, d.domain)
	ips, err := net.LookupIP(query)

	var retval int64 = 0
	if err == nil && len(ips) > 0 && d.matchRx.MatchString(ips[0].String()) {
		retval = 1
	}
	return &tengo.Int{Value: retval}, nil
}
