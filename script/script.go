package script

import (
	"context"
	"io/ioutil"
	"log"
	"sync"

	tengo "github.com/d5/tengo/v2"
	"github.com/d5/tengo/v2/stdlib"

	"git.autistici.org/ai3/tools/iprep/ext"
)

// A Script is a compiled Tengo script that will be executed on every
// request in order to provide a final reputation score for an IP.
//
// The script will have a few global variables available:
//
// 'ip' is the IP being looked up
//
// 'counts' is a map[string]int64 of counters returned from the
// database in the chosen time interval
//
// 'interval' is the duration in seconds of the time interval chosen
// for this evaluation, so one can compute rates
//
// To return the final reputation score (in the range 0 - 1), the
// script should set the 'score' variable.
//
type Script struct {
	compiled *tengo.Compiled
}

// LoadScript creates a new Script by loading its source from the
// specified file.
func LoadScript(path string) (*Script, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return NewScript(data)
}

var globalVars = []string{
	"ip", "score", "counts", "interval", "ext",
}

// NewScript creates a new Script with the given source.
func NewScript(src []byte) (*Script, error) {
	s := tengo.NewScript(src)

	// Define all the global variables (input and output), so that
	// they can be set later on the compiled object. The object
	// type we use here does not matter.
	for _, v := range globalVars {
		if err := s.Add(v, 0); err != nil {
			return nil, err
		}
	}

	// Load the Tengo standard library.
	s.SetImports(stdlib.GetModuleMap(stdlib.AllModuleNames()...))

	// Compile the script. We're going to Clone the compiled
	// version on every request.
	c, err := s.Compile()
	if err != nil {
		return nil, err
	}

	return &Script{compiled: c}, nil
}

// RunIP evaluates the script once with the provided context and
// returns the resulting score.
func (s *Script) RunIP(ctx context.Context, ip string, counts map[string]int64, intervalSecs float64, extSrcs map[string]ext.ExternalSource) (float64, error) {
	c := s.compiled.Clone()

	// Set the global variables that constitute the script
	// execution context. We expect no errors here even if the
	// script does not reference some of these variables, as we
	// have pre-defined them in NewScript.
	if err := c.Set("ip", ip); err != nil {
		return 0, err
	}
	if err := c.Set("score", 0.0); err != nil {
		return 0, err
	}
	if err := c.Set("interval", intervalSecs); err != nil {
		return 0, err
	}
	if err := c.Set("ext", extLookupFunc(extSrcs)); err != nil {
		return 0, err
	}
	if err := c.Set("counts", intMap(counts)); err != nil {
		return 0, err
	}

	// Run the script and extract the result from the global
	// variable 'score'. Acceptable values for score are anything
	// that can be converted to float by Tengo.
	if err := c.RunContext(ctx); err != nil {
		return 0, err
	}
	return c.Get("score").Float(), nil
}

type Manager struct {
	path string

	mx  sync.Mutex
	cur *Script
}

func NewManager(path, defaultScript string) (*Manager, error) {
	m := &Manager{path: path}
	var err error

	m.cur, err = LoadScript(path)
	if err != nil {
		log.Printf("script error: %s: %v", path, err)
		m.cur, err = NewScript([]byte(defaultScript))
		if err != nil {
			return nil, err
		}
	}

	return m, nil
}

func (m *Manager) Reload() {
	s, err := LoadScript(m.path)
	if err != nil {
		log.Printf("reload: %s: %v", m.path, err)
		return
	}

	m.mx.Lock()
	m.cur = s
	m.mx.Unlock()
	log.Printf("loaded new script from %s", m.path)
}

func (m *Manager) Script() *Script {
	m.mx.Lock()
	defer m.mx.Unlock()
	return m.cur
}
