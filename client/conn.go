package client

import (
	"context"
	"io"
	"log"

	ippb "git.autistici.org/ai3/tools/iprep/proto"
	"google.golang.org/grpc"
)

type Client interface {
	Submit(context.Context, []*ippb.Event, *ippb.Aggregate) error
	GetScore(context.Context, string) (*ippb.GetScoreResponse, error)
	GetAllScores(context.Context, float32) (<-chan *ippb.GetScoreResponse, error)
}

func New(conn *grpc.ClientConn) Client {
	return newClient(conn)
}

type rpcClient struct {
	stub ippb.IpRepClient
}

func newClient(conn *grpc.ClientConn) *rpcClient {
	return &rpcClient{
		stub: ippb.NewIpRepClient(conn),
	}
}

func (c *rpcClient) Submit(ctx context.Context, events []*ippb.Event, aggr *ippb.Aggregate) error {
	var req ippb.SubmitRequest
	req.Events = events
	if aggr != nil {
		req.Aggregates = append(req.Aggregates, aggr)
	}
	_, err := c.stub.Submit(ctx, &req)
	return err
}

func (c *rpcClient) GetScore(ctx context.Context, ip string) (*ippb.GetScoreResponse, error) {
	return c.stub.GetScore(ctx, &ippb.GetScoreRequest{Ip: ip})
}

func (c *rpcClient) GetAllScores(ctx context.Context, threshold float32) (<-chan *ippb.GetScoreResponse, error) {
	stream, err := c.stub.GetAllScores(ctx, &ippb.GetAllScoresRequest{Threshold: threshold})
	if err != nil {
		return nil, err
	}

	ch := make(chan *ippb.GetScoreResponse, 100)

	go func() {
		defer close(ch)
		for {
			entry, err := stream.Recv()
			if err == io.EOF {
				break
			} else if err != nil {
				log.Printf("error in GetAllScores stream: %v", err)
				break
			}
			ch <- entry
		}
	}()

	return ch, nil
}
